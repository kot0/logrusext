package logrusext

import (
	"fmt"
	"sync"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"gitlab.com/kot0/tools"
)

type LokiLogHook struct {
	AppName  string `yaml:"app_name"`
	Endpoint string `yaml:"endpoint"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Debug    bool   `yaml:"debug"`

	m         sync.Mutex
	logBuffer []*Stream

	logFlusherRunning bool
}

func (h *LokiLogHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (h *LokiLogHook) logFlusher() {
	// Flush logs every 0.5s if log buffer is not empty
	for range time.NewTicker(500 * time.Millisecond).C {
		h.m.Lock()
		if len(h.logBuffer) != 0 {
			logs := h.extractLogsFromLogBuffer()
			go h.sendLogs(logs)
		}
		h.m.Unlock()
	}
}

func (h *LokiLogHook) sendLogs(logs []*Stream) {
	if h.Debug {
		fmt.Println("Sending (" + cast.ToString(len(logs)) + ") logs...")
	}

	startTime := time.Now()
	for time.Now().Sub(startTime) < 60*time.Second {
		resp, err := resty.New().SetCloseConnection(true).SetTimeout(10*time.Second).SetDisableWarn(true).R().
			SetBasicAuth(h.Username, h.Password).
			SetHeader("Content-Type", "application/json").
			SetBody(Streams{Streams: logs}).
			Post(h.Endpoint + `/loki/api/v1/push`)
		if tools.CheckError(err) || resp.StatusCode() != 204 {
			fmt.Println("Error: send logs failed", err, resp.StatusCode(), resp.String())
			time.Sleep(3 * time.Second)
			continue
		}
		break
	}

	if h.Debug {
		fmt.Println("Sent ("+cast.ToString(len(logs))+") logs! took:", cast.ToString(time.Now().Sub(startTime).Milliseconds())+"ms")
	}
}

func (h *LokiLogHook) extractLogsFromLogBuffer() []*Stream {
	streams := make([]*Stream, len(h.logBuffer))
	copy(streams, h.logBuffer)
	h.logBuffer = nil

	return streams
}

func (h *LokiLogHook) Fire(e *logrus.Entry) error {
	// Start log flusher if it's not running
	h.m.Lock()
	if !h.logFlusherRunning {
		go h.logFlusher()
		h.logFlusherRunning = true
	}
	h.m.Unlock()

	// Build log entry and append it in a buffer
	logAttributes := map[string]string{
		"file": e.Caller.File + ":" + cast.ToString(e.Caller.Line),
		"func": e.Caller.Function,
	}
	for k, v := range e.Data {
		logAttributes[k] = cast.ToString(v)
	}

	h.m.Lock()
	h.logBuffer = append(h.logBuffer, &Stream{
		Stream: map[string]string{
			"name":  h.AppName,
			"level": e.Level.String(),
		},
		Values: [][]interface{}{{cast.ToString(e.Time.UTC().UnixNano()), e.Message, logAttributes}},
	})
	h.m.Unlock()

	return nil
}

type Streams struct {
	Streams []*Stream `json:"streams"`
}

type Stream struct {
	Stream map[string]string `json:"stream"`
	Values [][]interface{}   `json:"values"`
}
