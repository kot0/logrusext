module gitlab.com/kot0/logrusext

go 1.20

require (
	github.com/go-resty/resty/v2 v2.10.0
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/cast v1.5.0
	gitlab.com/kot0/tools v1.14.1
)

require (
	github.com/Pallinder/go-randomdata v1.2.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
