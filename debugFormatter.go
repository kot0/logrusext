package logrusext

import (
	"bytes"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"gitlab.com/kot0/tools"
)

type DebugFormatter struct {
}

func (f *DebugFormatter) Format(e *logrus.Entry) ([]byte, error) {
	var output bytes.Buffer

	output.WriteString(e.Time.Format(tools.TimeFormat) + " " + e.Level.String() + ":")

	// If log has fields
	if len(e.Data) != 0 {
		output.WriteString(" ")
		output.WriteString("(")
		for k, v := range e.Data {
			output.WriteString(k + ":" + cast.ToString(v))
			output.WriteString(" ")
		}
		output.Truncate(output.Len() - 1) // trim latest space
		output.WriteString(")")
	}

	output.WriteString(" ")
	output.WriteString(e.Message)

	output.WriteRune('\n')

	return output.Bytes(), nil
}
